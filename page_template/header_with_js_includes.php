<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
//-->
<header>
	<title>Scatter</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $css; ?>">
	<h1 id="scatter" >&nbsp;<img src="scatter.png" id="scatter_image" />catter</h1>
	<script src="posts/post.js"></script>
	<script src="posts/get_posts.js"></script>
	<script src="messaging/open_messaging.js"></script>
	<script src="users/search_users.js"></script>
	<script src="users/get_friends.js"></script>
	<script src="posts/post_comment.js"></script>
	<script src="posts/get_comments.js"></script>
	<script src="posts/get_more_posts.js"></script>
	<script src="misc/jquery-2.1.0.min.js"></script>
    	<script src="profilepage/getUserProfilePage.js"></script>
	<script src="posts/like_dislike.js"></script>
	<script src="misc/notifications.js"></script>
	<a href="login/logout.php" id="logout">logout</a>
	<img src="misc/message_box.png" id="message_box_image" onClick="window.location='./messaging'" />
	<?php
		include $location.'login/mysql_login.php';
		$current_user=$_SESSION['user_id'];
		$sql="SELECT * FROM user_$current_user"."_notifications WHERE viewed=0";
		$result= mysqli_query($mysqli,$sql);
		$count=$result->num_rows;
		if($count>0){
			echo'<img src="misc/notifications_active.png" id="notifications_image" onclick="get_notifications();"/>';
		}
		else{
			echo'<img src="misc/notifications.png" id="notifications_image" onclick="get_notifications();"/>';
		}
	?>
	<img src="misc/friends.png" id="friends_image" onclick="get_friends();"/>
	<script>message_user_id=0;message_minimized=false;div_number=1;check=true;</script>

</header>

<!DOCTYPE html> 
<html>
<head>
<base href="../">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
//-->
<script>type="";</script>
<?php 
$type="";
$css="css/main.css";
include "../page_template/session_check.php";
?>
<?php
$location="../";
include "../page_template/header_with_js_includes.php";
?>
<script src="profilepage/get_posts.js"></script>
<body>
<?php
include "../page_template/ToC.php";
include "../page_template/friends_with_js.php";
include "../page_template/message_box.php";
include "../page_template/feed.php";
include "../page_template/user_search_input.php";
include "../page_template/post_input.php";
?>


</body>
<script src="misc/infinite_scroll.js"></script>
</html>

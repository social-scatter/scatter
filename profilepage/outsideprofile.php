




<!DOCTYPE html> 
<html>
<head>
<base href="../">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
//-->
<script>type="";</script>
<script>message_user_id=0;message_minimized=false;div_number=1;check=true;</script>
<?php 
$type="";
$css="css/main.css";
include "../page_template/session_check.php";
include "../login/mysql_login.php";

$current_user = $_SESSION["user_id"];
	
$sql= "SELECT * FROM user_$current_user"."_aboutMyself";
$result= mysqli_query($mysqli,$sql); 

//echo"$result";
?>
<?php
$location="../";
include "../page_template/header_without_js_includes.php";
?>
<header>
	<script src="posts/post.js"></script>
	<script src="messaging/open_messaging.js"></script>
	<script src="users/search_users.js"></script>
	<script src="users/get_friends.js"></script>
	<script src="posts/post_comment.js"></script>
	<script src="posts/get_comments.js"></script>
	<script src="misc/jquery-2.1.0.min.js"></script>
    	<script src="profilepage/getUserProfilePage.js"></script>
	<script src="posts/like_dislike.js"></script>
	<script src="misc/notifications.js"></script>
</header>
<script src="profilepage/get_posts2.js"></script>
<body>
<?php
include "../page_template/ToC.php";
include "../page_template/friends_with_js.php";
include "../page_template/message_box.php";
?>
<div id="feed">
<div id="spacer">
</div>
<br />

<br />
<br />
<div id="container">
<div id="1">
<script>
	get_posts(20,-1, "","",<?php echo $_GET['user_id']; ?>);
</script>
</div>
</div>

<script>
function getMorePosts(){
	var oldStuff = document.getElementById("container").innerHTML;//modifies container div to contain the posts
	get_posts(2,true, oldStuff,"",<?php echo $_GET['user_id']; ?>);

}

</script>
<br />
<br />

<br />

</div>
<?php
include "../page_template/user_search_input.php";
include "../page_template/post_input.php";
?>
</body>
<script src="misc/infinite_scroll.js"></script>
</html>
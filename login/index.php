<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<?php
//This is the functionallity behind the main page rather the user is logged in or not.
// This is the page functionallity that shows the users posts and such and also shows the login page
session_start();
    $_SESSION["username"] = "";
	if($_SESSION["username"] != ""){
		header("location: ../");
	}
?>

<html>
<head>
<link rel="icon" href="../favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
</head>
<header>
	<title>Scatter</title>
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<h1 id="scatter" style="position:fixed;top:-14px;">&nbsp;<img src="../scatter.png" width="64" height="64" style="vertical-align:middle;" />catter</h1>
	<script src="post.js"></script>
	<script src="get_posts.js"></script>
	

</header>
<body>
<div id="spacer2"></div>
<div id="login" style="text-align:center;width:20%;float:left;height:80%;border-right:1px solid black;">
    <h2>Please login to access Scatter.</h2>
    <br />
	<form name="login_form" method="post" action="checklogin.php">
		<h3>Username (email)<h3>
		<input name="username" type="text" id="username">
		<br />
		<h3>Password</h3>
		<input name="password" type="password" id="password">
		<br />
		<br />
		<br />
		<input type="submit" name="Submit" value="Login">
	</form>

	<a href="create_user.html">Create new user</a>
</div>
<div id="pretty_stuff" style="float:left;text-align:center;width:70%;margin-left:5%;">
<h1>Welcome to Scatter</h1>
<h2>A social network made by <a href="../info">you!</a></h2>
<img src="Scatter.png" style="width:90%;" />

</div>

</body>

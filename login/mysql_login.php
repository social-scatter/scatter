<?php
/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
/*the purpose of this document is to recieve a connection with the mysql database, 
it is currently set up for localhost or 127.0.0.1 on your machine.  
HOWEVER! remember when running this on your machin to change the comment "insert_password_here"
to the root password of your mysql database.

*/
$host="localhost"; // Host name 
$mysql_username="root"; // Mysql username
$mysql_password="/*insert_password_here*/"; // Mysql password 
$db_name="scatter"; // Database name

$mysqli = mysqli_connect($host, $mysql_username, $mysql_password, $db_name); 

if (mysqli_connect_errno()){
  echo "Failed to connect to MySQL: " . mysqli_connect_error();//if the site cannot connect or authenticate into the database it will throw the apropriate error.
}//end if statement

?>

<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<?php
//This document is executed by "checklogin.php" when a sucessful login has occured.  A "session" is started in the 
//users browser keeping them logged in 
	session_start();
	echo($_SESSION["username"]." has succesfully logged in. You will now be redirected.");
	header("location:../");
?>

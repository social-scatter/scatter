<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<?php
//This is the functionallity behind the main page rather the user is logged in or not.
// This is the page functionallity that shows the users posts and such and also shows the login page
session_start();
    $_SESSION["username"] = "";
	if($_SESSION["username"] != ""){
		header("location: ../");
	}
?>

<html>
<header>
	<title>Scatter</title>
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<h1>&nbsp;Scatter</h1>
	<script src="post.js"></script>
	<script src="get_posts.js"></script>
	<a href="login/logout.php" id="logout">logout</a>

</header>
<body>
<div id="spacer3" style="height:20%;width:100%;"></div>
<div id="Text" style="text-align:center;width:50%;float:left;height:80%;">
	<h2>What do you mean by Scatter is a social network made by you?</h2>
	<p>Not only is Scatter a social network it's also an open source project. What this means is that anyone can help develop it and there are always features being added. Even if you're not a programmer you can help the project by posting bugs and enhancement requests on our issue page. If that's not your thing we also appreciate it when you spread the word about Scatter.<br />To check out our development page head to <a href="https://bitbucket.org/pfeiferj/scatter">https://bitbucket.org/pfeiferj/scatter</a></p>
</div>
<div id="pretty_stuff" style="float:left;text-align:center;width:48%;margin-left:1%;">
<img src="development.png" style="width:100%;" />

</div>

</body>

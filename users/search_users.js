/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function user_search(){
	search_users = new XMLHttpRequest();
	search_users.onreadystatechange=function()
 	 {
 	 	if (search_users.readyState==4 && search_users.status==200)//4 means page did everything, and 200 means the page exists
	 	   {
			document.getElementById("replace").innerHTML=search_users.responseText;
	  	   }
 	 }
	search_users.open("POST","users/search_users.php",true);
	search_users.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	search_text=document.getElementById("user_search_input").value;
	search_users.send("first_name="+search_text);
	
	//alert("posted");
	//document.getElementById("container").innerHTML=email.responseText;
}
function follow(user_id){
	follow_user = new XMLHttpRequest();
	follow_user.onreadystatechange=function()
 	 {
 	 	if (follow_user.readyState==4 && follow_user.status==200)
	 	   {
			location.reload(true);
	  	   }
 	 }
	follow_user.open("POST","users/add_friend.php",true);
	follow_user.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	follow_user.send("user_id="+user_id);

}

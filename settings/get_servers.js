/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function get_servers(){
	//alert('hit this');
	servers = new XMLHttpRequest();
	servers.onreadystatechange=function()
 	 {
 	 	if (servers.readyState==4 && servers.status==200)
	 	   {
			document.getElementById('container').innerHTML=servers.responseText;
	  	   }
 	 }
	servers.open("POST","settings/get_servers.php",true);
	servers.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	servers.send();
}


/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function get_server_requests(){
	servers = new XMLHttpRequest();
	servers.onreadystatechange=function()
 	 {
 	 	if (servers.readyState==4 && servers.status==200)
	 	   {
			document.getElementById('container').innerHTML=servers.responseText;
	  	   }
 	 }
	servers.open("POST","settings/get_server_requests.php",true);
	servers.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	servers.send();
}


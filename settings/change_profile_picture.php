<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<script>type="";</script>
<?php 
$type="";
$css="css/main.css";
include "../page_template/session_check.php";
?>
<html>
<base href = "../">
<?php
include "../page_template/header_with_js_includes.php";
?>

<body>
<div id="ToC">
	<div id="spacer2"></div>
	<div id="friend">
	<p onClick="window.location ='./';">Home</p>
	</div>
	<div id="friend">
	<p onClick="window.location='settings/change_profile_picture.php'">Change Profile Picture</a></p>
	</div>
	<div id="friend">
	<p onClick="window.location='settings/changepassword.php'">Change Password</a></p>
	</div>

</div>
<?php
include "../page_template/friends_with_js.php";
include "../page_template/message_box.php";
?>
<div id="feed">
<div id="spacer">
</div>
<br />

<br />
<br />
<div id="container">
<center>
<h2>Set Profile Picture</h2>
<form action="settings/set_profile_picture.php" method="post" enctype="multipart/form-data">
<input type="file" name="file" id="file" value="Choose Photo" style="border:1px solid grey"/>
<br />
<input type="submit" name="submit" value="Set Profile Picture" />
</form>
</center>
</div>
<div id="spacer4"></div>
<br />
<br />

<br />

</div>
<?php
include "../page_template/user_search_input.php";
?>


</body>
<script src="misc/infinite_scroll.js"></script>
</html

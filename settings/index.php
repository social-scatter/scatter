<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<script>type="";</script>
<?php 
$type="";
$css="css/main.css";
include "../page_template/session_check.php";
if($_SESSION["username"] == "scatter"){
	header("location:server_settings.php");
}
?>
<html>
<base href = "../">
<?php
include "../page_template/header_with_js_includes.php";
?>

<body>
<div id="ToC">
	<div id="spacer2"></div>
	<div id="friend">
	<p onClick="window.location ='./';">Home</p>
	</div>
	<div id="friend">
	<p onClick="window.location='settings/change_profile_picture.php'">Change Profile Picture</a></p>
	</div>
	<div id="friend">
	<p onClick="window.location='settings/changepassword.php'">Change Password</a></p>
	</div>

</div>
<?php
include "../page_template/friends_with_js.php";
include "../page_template/message_box.php";
?>
<div id="feed">
<div id="spacer">

</div>
<br />

<br />
<br />
<div id="container">
<form name="userInfo" action="settings/aboutMyself.php" method="post" id="aboutMe">
About myself: <input type="text" name="aboutMyself" style="width:200px; height:80px;"><br>
<input type="radio" name="sex" value="male">Male<br>
<input type="radio" name="sex" value="female">Female<br>
<input type="radio" name="sex" value="undecided">Undecided<br>
<input type="submit" name="Submit" value="Submit">
</form>
</div>
<div id="spacer4"></div>
<br />
<br />

<br />

</div>
<?php
include "../page_template/user_search_input.php";
?>


</body>
<script src="misc/infinite_scroll.js"></script>
</html

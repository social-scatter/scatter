<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<?php
	include "../API/server_api/request_integration.php";
	include "../misc/sanitize.php";
	$location=$_POST['location'];
	$location_sanitized=sanitize($location);
	echo $location_sanitized;
	echo request_integration($location_sanitized);
	header("location:./");
?>

<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<script>type="";</script>
<?php 
$type="";
$css="css/main.css";
include "../page_template/session_check.php";
?>
<html>
<script src="get_servers.js"></script>
<script src="get_server_requests.js"></script>
<script src="request_integration.js"></script>
<base href = "../">
<?php
include "../page_template/header_with_js_includes.php";
?>

<body>
<div id="ToC">
	<div id="spacer2"></div>
	<div id="friend">
	<p onClick="window.location ='./';">Home</p>
	</div>
	<div id="friend">
	<p onClick="get_servers();">Integrated Servers</p>
	</div>
	<div id="friend">
	<p onClick="get_server_requests();">Open Requests</p>
	</div>
	<div id="friend">
	<p onClick="request_integration_page();">Request Integration</p>
	</div>
	<div id="friend">
	<p onClick="window.location='settings/change_profile_picture.php'">Change Profile Picture</a></p>
	</div>
	<div id="friend">
	<p onClick="window.location='settings/changepassword.php'">Change Password</a></p>
	</div>

</div>
<?php
include "../page_template/friends_with_js.php";
include "../page_template/message_box.php";
?>
<div id="feed">
<div id="spacer">
</div>
<br />
<br />
<div id="container">
</div>
<div id="spacer4"></div>
<br />
<br />

<br />

</div>
<?php
include "../page_template/user_search_input.php";
?>


</body>
<script src="misc/infinite_scroll.js"></script>
</html

<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<?php
	include "../API/server_api/integrate.php";
	include "../misc/sanitize.php";
	$location=$_POST['location'];
	$N = count($location);
	for($i=0;$i<$N;$i++){
		$location_sanitized=sanitize($location[$i]);
		echo confirm_integration($location_sanitized);
	}
?>

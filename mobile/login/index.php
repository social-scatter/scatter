<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
-->
<?php
//This is the functionallity behind the main page rather the user is logged in or not.
// This is the page functionallity that shows the users posts and such and also shows the login page
session_start();
    $_SESSION["username"] = "";
	if($_SESSION["username"] != ""){
		header("location: ../");
	}
?>

<html>
<head>
<meta name="viewport" content="width=device-width" initial-scale="1" minimum-scale="1">
<link rel="icon" href="../favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
</head>
<header>
	<title>Scatter</title>
	<link rel="stylesheet" type="text/css" href="../../css/main.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<h1 id="scatter" >&nbsp;<img src="../../scatter.png" id="scatter_image" />catter</h1>
	<script src="post.js"></script>
	<script src="get_posts.js"></script>
	

</header>
<body>
<div id="login" style="text-align:center;width:100%;float:center;height:80%;">
    <div id="spacer"></div>
    <h1>Welcome to Scatter</h1>
    <h2>Please login to access Scatter.</h2>
    <br />
	<form name="login_form" method="post" action="checklogin.php">
		<h3>Username (email)<h3>
		<input name="username" type="text" id="username">
		<br />
		<h3>Password</h3>
		<input name="password" type="password" id="password">
		<br />
		<br />
		<br />
		<input type="submit" name="Submit" value="Login">
	</form>

	<a href="create_user.html">Create new user</a>
</div>

</body>

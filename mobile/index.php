<!DOCTYPE html> 
<html>
<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
//-->
<base href = "../">
<meta name="viewport" content="width=device-width" initial-scale="1" minimum-scale="1">
<script>type="";</script>
<?php 
$type="";
$css="css/main.css";
include "../page_template/session_check.php";
?>
<?php
include "../page_template/header_with_js_includes.php";
?>
<head>
<link rel="stylesheet" type="text/css" href="mobile/css/main.css">
</head>
<body>
<?php
include "../page_template/feed.php";
include "../page_template/post_input.php";
?>


</body>
<script src="misc/infinite_scroll.js"></script>
</html>

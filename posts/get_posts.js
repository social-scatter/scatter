/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function get_posts(number, compress, oldPosts, type){
	posts = new XMLHttpRequest();
	posts.onreadystatechange=function()
 	 {
 	 	if (posts.readyState==4 && posts.status==200)
	 	   {
			document.getElementById(div_number).innerHTML=posts.responseText;
			div_number = div_number + 1;
			check=true;
	  	   }
 	 }
	posts.open("POST","posts/get_posts.php",true);
	posts.setRequestHeader("Content-type","application/x-www-form-urlencoded");//don't know what this is Mr.Pfeifer will explain
	posts.send("number="+number+"&compress="+compress+"&type="+type);
}
//above function basically just makes a call to the post.php and get_post.php to get all the relevant data

<?php
/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
//this document is a bit of a mess but bear with me
//it's purpose is pretty much described in the name of the file, it get's the relevant posts to the user
session_start();
include "../login/mysql_login.php";
$location = $_POST["number"];
$location++;
$thisMany = 1;
$current_user = $_SESSION["user_id"];
$startLocation = $location+$thisMany;
$type = $_POST['type'];
$compress = 'compress';

$tbl_name="posts"; // Table name


$sql="SELECT * FROM $tbl_name WHERE user_id IN (select user_id FROM user_$current_user"."_friends) AND number < $location AND comments IS NULL ORDER BY number DESC";

$result= mysqli_query($mysqli,$sql);
//-------------find post--------------//
while($row = mysqli_fetch_array($result)){
	if($type != "photo"){
		$i++;
		if($i>$thisMany){
			break;
		}
	}
	$number=$row['number'];
	$likes=$row['likes'];
	$dislikes=$row['dislikes'];
//-------------get likes/dislikes--------------//
	$sql2 = "SELECT * FROM like_dislike WHERE post=$number AND user_id=$current_user";
	$result2= mysqli_query($mysqli,$sql2);
	if($row2 = mysqli_fetch_array($result2)){
		if($row2['liked']==1){
			$css1="color:green;";
			$css2="color:grey;";
		}
		else if($row2['disliked']==1){
			$css2="color:red;";
			$css1="color:grey;";
		}

	}
	else{
		$css1="color:grey;";
		$css2="color:grey;";
	}
	$likedislike='<span id="likedislike"><span id="like" onclick="like_dislike('."'like'".', '.$number.');"><span id="like_'.$number.'" style="'.$css1.'"><span style="font-size:7pt;">'.$likes.'</span> &#x2713;</span></span>/ <span id="dislike" onclick="like_dislike('."'dislike'".', '.$number.');"><span id="dislike_'.$number.'" style="'.$css2.'">x &nbsp;<span style="font-size:7pt;">'.$dislikes.'</span></span></span></span>';
	$tbl_name2 = "user_".$row['user_id'];
	$timestamp = $row['time'];
	$user_id = $row['user_id'];

	$sql="SELECT * FROM $tbl_name WHERE comments=$number ORDER BY number DESC";
	$result4=mysqli_query($mysqli,$sql);
//-------------get comment--------------//
	if($row4=mysqli_fetch_array($result4)){
		$sql="SELECT * FROM user_".$row4['user_id']." WHERE time='".$row4['time']."'";
		$result5=mysqli_query($mysqli,$sql);
		$row5=mysqli_fetch_array($result5);
		$sql="SELECT * from users where user_id='".$row4['user_id']."'";
		$result6=mysqli_query($mysqli,$sql);
		$row6=mysqli_fetch_array($result6);
		$comment='<div id="comment_text"><span id="comment_name">'.ucfirst($row6['first_name']).' '.ucfirst($row6['last_name']).'</span><br /></div><div id="grey">'.$row5['post'].'</div></div>';

		$comment='<div id="comment_text_'.$number.'"><div id="post" style="width:90%;margin-left:5%"><div id="post_'.$number.'"><div id="person"><p onClick="getUserProfilePage('.$user_id.');">'.ucfirst($row6['first_name']).' '.ucfirst($row6['last_name']).'</p></div><div id="time"><p style="color:grey;"></p></div><br /><p id="shift">'.$row5['post'].'</p></div></div></div><div id="break" style="width:100%;height:1px;margin: 0 0 -1px;clear:both;"></div>';		
		
		$view_comments='<span id="view_comments"><span id="view_comments_text" onclick="get_comments('.$number.')">view comments ('.$result4->num_rows.')</span></span>';
	}
	else{
		$comment="";
		$view_comments="";
	}
//-------------get post text--------------//
	$sql="SELECT * FROM $tbl_name2 where time='$timestamp'"; //pull post

	$result2= mysqli_query($mysqli,$sql);

	$row2 = mysqli_fetch_array($result2);

	$post = $row2['post'];
	$image_location = $row2['image'];
//-------------get post time--------------//
	$time = $row2['time'];
	$split_date_time = explode (" ", $time);
	$date=$split_date_time[0];
	$split_date=explode("-",$date);
	$current_date=getdate();
	$current_date_formatted=$current_date['year']."-".$current_date['mon']."-".$current_date['mday'];
	$time=$split_date_time[1];
	$split_time=explode(":",$time);
	if($current_date['year']-$split_date[0]>0||$current_date['mon']-$split_date[1]>0||$current_date['mday']-$split_date[2]>0){
		$time=$split_date[1]."-".$split_date[2]." ".$split_date[0];
	}
	else{
		$ampm = "AM";
		if($split_time[0]>12){
			$split_time[0] = $split_time[0]-12;
			$ampm = "PM";
		}
		$time=$split_time[0].":".$split_time[1]." ".$ampm;
	}
//-------------get name--------------//
	$sql="SELECT * FROM users where user_id='$user_id'";
	$result3= mysqli_query($mysqli,$sql);
	$row3 = mysqli_fetch_array($result3);
	$first_name = ucfirst($row3['first_name']);
	$last_name = ucfirst($row3['last_name']);
	$profile_picture=$row3['profile_picture'];
	if($profile_picture !=""){
		$profile_picture = $profile_picture.'.compressed';
		$profile_pictureHTML='<img src="misc/get_image.php?img='.$profile_picture.'" id="profile_picture" />';
	}
	else
		$profile_pictureHTML='';
//-------------form post--------------//
	if($image_location == "" && $type != "photo"){
		echo '<div id="post"><div id="post_'.$number.'"><div id="person"><p onClick="getUserProfilePage('.$user_id.');">'.$profile_pictureHTML.' '.$first_name.' '.$last_name.'</p></div><div id="time"><p id="grey">'.$time.'</p></div><br /><p id="shift">'.$post.'</p>'.$comment.'<div id="comment"><span style="color:grey;position:absolute;left:2px;" id="comment_'.$number.'" onclick="open_commenting('.$number.')">&nbsp;comment</span>'.$likedislike.$view_comments.'</div></div></div>'; //display post
	}
	else if($type != "text" && $image_location != ""){
		if($type == "photo"){
			$i++;
			if($i>$thisMany){
				break;
			}
		}
		if($compress){
			$image_location_compressed = $image_location.'.compressed';
		}
		echo '<div id="post"><div id="post_'.$number.'"><div id="person"><p onClick="getUserProfilePage('.$user_id.');">'.$profile_pictureHTML.' '.$first_name.' '.$last_name.'</p></div><div id="time"><p id="grey">'.$time.'</p></div><br /><p id="shift">'.$post.'</p><a href="misc/image.php?img='.$image_location.'"><img src="misc/get_image.php?img='.$image_location_compressed.'" id="image" /></a><br /><br />'.$comment.'<div id="comment""><span style="color:grey;position:absolute;left:2px;" id="comment_'.$number.'" onclick="open_commenting('.$number.')">&nbsp;comment</span>'.$likedislike.$view_comments.'</div></div></div>';
	}		
	
} 
//-------------set location--------------//
$_SESSION['post_location']=$number;
echo '<div id="'.$_SESSION['div_number'].'"></div>';

$_SESSION['div_number']=$_SESSION['div_number']+1;


?>

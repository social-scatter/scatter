<!DOCTYPE html> 
<html>
<base href = "../">
<head>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
<!-- 
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
//-->
<script>type="";</script>
<script src="posts/get_post.js"></script>
<?php 
$type="";
$css="css/main.css";
include "../page_template/session_check.php";
?>
<?php
include "../page_template/header_with_js_includes.php";
?>

<body>
<?php
include "../page_template/ToC.php";
include "../page_template/friends_with_js.php";
include "../page_template/message_box.php";
include "../page_template/feed.php";
include "../page_template/user_search_input.php";
include "../page_template/post_input.php";
echo "<script>get_post('".$_GET['number']."');</script>";
?>


</body>
</html>

<?php
/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
//make sure user is logged in
session_start();
include "../login/mysql_login.php";
include "../misc/sanitize.php";
include "../misc/php_post.php";
include "../API/server_api/make_post.php";
include "../misc/make_notification.php";


	function compress($source, $destination, $quality) {
		echo "hit this";
		$info = getimagesize($source);

		if ($info['mime'] == 'image/jpeg') 
			$image = imagecreatefromjpeg($source);

		elseif ($info['mime'] == 'image/gif') 
			$image = imagecreatefromgif($source);

		elseif ($info['mime'] == 'image/png') 
			$image = imagecreatefrompng($source);

		echo "hit this";

		imagejpeg($image, $destination, $quality);

		return $destination;
	}


$tbl_name="users"; // Table name

$username=$_SESSION["username"]; 
$current_user=$_SESSION["user_id"];
$post_number=$_POST["post_number"];
$file=false;


$allowedExts = array("gif", "jpeg", "jpg", "png");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/jpg")
|| ($_FILES["file"]["type"] == "image/pjpeg")
|| ($_FILES["file"]["type"] == "image/x-png")
|| ($_FILES["file"]["type"] == "image/png"))
&& in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    {
    echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
    }
  else
    {
    $name = $_FILES["file"]["name"];
    echo "Type: " . $_FILES["file"]["type"] . "<br>";
    echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
    echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

    if (file_exists("/srv/Scatter/$current_user/" . $_FILES["file"]["name"]))
      {
      echo $_FILES["file"]["name"] . " already exists. ";
      }
    else
      {
      move_uploaded_file($_FILES["file"]["tmp_name"],
      "/srv/Scatter/$current_user/" . $_FILES["file"]["name"]);
      echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
	$file=true;
	$source_img = '/srv/Scatter/'.$current_user.'/'.$name;
	$destination_img = '/srv/Scatter/'.$current_user.'/'.$name.'.compressed';

	$d = compress($source_img, $destination_img, 30);
      }
    }
  }
else
  {
  echo "Invalid file";
  }


$sql="SELECT * FROM $tbl_name WHERE username='$username'";

$result=$mysqli->query($sql);
$row = mysqli_fetch_array($result); 
$user_id=$row['user_id'];
$count=$result->num_rows;
$post = $_POST["post"];
$post = sanitize($post);
$post = strip_tags($post);
echo($post);
if($count==1){
	if($file){
		$sql="INSERT INTO user_$user_id VALUES('$post',NULL,NULL,'$current_user/$name')";
	}
	else{
		$sql="INSERT INTO user_$user_id VALUES('$post',NULL,NULL,NULL)";
	}
	echo $sql;
	$result=$mysqli->query($sql);
	if($result){//if query was a success
		$sql="SELECT * FROM user_$user_id ORDER BY number DESC";
		$result=$mysqli->query($sql);
		$row = mysqli_fetch_array($result); 
		$timestamp=$row['time'];
		echo $post_number;
		if($post_number!=""){
			$sql="SELECT * FROM posts WHERE number=$post_number";
			$result=$mysqli->query($sql);
			$row = mysqli_fetch_array($result); 
			$notify_user_id=$row['user_id'];
			$sql="SELECT * FROM users WHERE user_id=$user_id";
			$result=$mysqli->query($sql);
			$row = mysqli_fetch_array($result);
			$name=$row['first_name'].' '.$row['last_name'];
			$sql="INSERT INTO posts VALUES('$user_id','$timestamp',NULL,$post_number,0,0)";
			make_notification($notify_user_id ,'New Comment', '<span onclick=\"window.location='."\'".'posts/?number='."$post_number\'".'">'.$name.' commented on your post</span>');
		}
		else{
			$sql="INSERT INTO posts VALUES('$user_id','$timestamp',NULL,NULL,0,0)";
		}
		$mysqli->query($sql);
		echo $sql;
	}
}
else{
	header("location:../login/");
}
header("location:../");
?>

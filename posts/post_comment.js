/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function open_commenting(post_number){
	document.getElementById('comment_'+post_number).innerHTML = "";
	var post_without_textbox = document.getElementById('post_'+post_number).innerHTML;
	document.getElementById('post_'+post_number).innerHTML = post_without_textbox + '<input type="textbox" id="post_'+post_number+'_textbox" placeholder="Comment" style="width:90%;" onkeydown="if (event.keyCode == 13) post_comment('+post_number+');"/>';
}
function post_comment(post_number){
	var text = document.getElementById('post_'+post_number+'_textbox').value;
	post_comment = new XMLHttpRequest();
	post_comment.open("POST","posts/post.php",false);
	post_comment.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	post_comment.send("post="+text+"&post_number="+post_number);
	location.reload(true);
	
	//alert("posted");
	//document.getElementById("container").innerHTML=email.responseText;
}

/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function get_comments(number){
	get_comments = new XMLHttpRequest();
	get_comments.open("POST","posts/get_comments.php",false);
	get_comments.setRequestHeader("Content-type","application/x-www-form-urlencoded");//don't know what this is Mr.Pfeifer will explain
	get_comments.send("number="+number);
	document.getElementById("comment_text_"+number).innerHTML=get_comments.responseText;
}
//above function basically just makes a call to the post.php and get_post.php to get all the relevant data

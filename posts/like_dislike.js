/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function like_dislike(type, number){
	like = new XMLHttpRequest();
	like.onreadystatechange=function()
 	 {
 	 	if (like.readyState==4 && like.status==200)
	 	   {
			response=like.responseText.split(",");
			document.getElementById('like_'+number).innerHTML='<span style="font-size:7pt;">'+response[0]+'</span> &#x2713;</span>';
			document.getElementById('dislike_'+number).innerHTML='x &nbsp;<span style="font-size:7pt;">'+response[1]+'</span>';
			if(response[0]==1){
				document.getElementById('like_'+number).style.cssText="color:green;";
			}
			else if(response[0]==0){
				document.getElementById('like_'+number).style.cssText="color:grey;";
			}
			if(response[1]==1){
				document.getElementById('dislike_'+number).style.cssText="color:red;";
			}
			else if(response[1]==0){
				document.getElementById('dislike_'+number).style.cssText="color:grey;";
			}
	  	   }
 	 }
	like.open("POST","posts/like_dislike.php",true);
	like.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	like.send("number="+number+"&type="+type);
}

/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function get_post(number){
	posts = new XMLHttpRequest();
	posts.onreadystatechange=function()
 	 {
 	 	if (posts.readyState==4 && posts.status==200)
	 	   {
			document.getElementById(1).innerHTML=posts.responseText;
	  	   }
 	 }
	posts.open("POST","posts/get_post.php",true);
	posts.setRequestHeader("Content-type","application/x-www-form-urlencoded");//don't know what this is Mr.Pfeifer will explain
	posts.send("number="+number);
}
//above function basically just makes a call to the post.php and get_post.php to get all the relevant data

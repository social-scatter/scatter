<?php
/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
session_start();
include "../login/mysql_login.php";
include "../misc/sanitize.php";
$type=sanitize($_POST['type']);
$number=sanitize($_POST['number']);
$sql="SELECT * FROM posts WHERE number=$number";
$result= mysqli_query($mysqli,$sql);
$user_id=$_SESSION['user_id'];
$update=true;
if($row = mysqli_fetch_array($result)){
	$likes=$row['likes'];
	$dislikes=$row['dislikes'];
	
	$sql = "SELECT * FROM like_dislike WHERE post=$number AND user_id=$user_id";
	$result= mysqli_query($mysqli,$sql);
	if($row2 = mysqli_fetch_array($result)){
		$update=false;
		if($row2['liked']==1){
			if($type=="dislike"){
				$dislikes=$dislikes+1;
				$update=true;
			}
			$likes=$likes-1;
		}
		else if($row2['disliked']==1){
			if($type=="like"){
				$likes=$likes+1;
				$update=true;
			}
			$dislikes=$dislikes-1;			
		}
		$sql="UPDATE posts SET time=time, likes=$likes, dislikes=$dislikes WHERE number=$number";
		mysqli_query($mysqli,$sql);
		$sql="DELETE FROM like_dislike WHERE post=$number AND user_id=$user_id";
		mysqli_query($mysqli,$sql);
	}
	else if($type==like){
		$likes=$likes+1;
	}
	else if($type==dislike){
		$dislikes=$dislikes+1;
	}


	if($type=="like"){
		$sql="UPDATE posts SET time=time, likes=$likes WHERE number=$number";
		$sql2="INSERT INTO like_dislike VALUES($number, $user_id, 1, 0)";
	}
	else if($type=="dislike"){
		$sql="UPDATE posts SET time=time, dislikes=$dislikes WHERE number=$number";
		$sql2="INSERT INTO like_dislike VALUES($number, $user_id, 0, 1)";
	}

	mysqli_query($mysqli,$sql);
	if($update==true){
		mysqli_query($mysqli,$sql2);
	}
	echo "$likes,$dislikes";
}

?>


/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
message_user_id=0;
function open_messaging_page(user_id){
	message_user_id=user_id
	document.getElementById("dynamic_message_input").innerHTML='<input type="text" name="message" id="message_input"  onkeydown="if (event.keyCode == 13) send_message_page('+user_id+');" placeholder="Type your message here">';

	messages = new XMLHttpRequest();
	messages.open("POST","messaging/get_messages.php",false);
	messages.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	messages.send("user_id="+user_id);	
	document.getElementById("container").innerHTML=messages.responseText;

	var objDiv = document.getElementById("feed");
	objDiv.scrollTop = objDiv.scrollHeight;	
}
function send_message_page(user_id){
	var message=document.getElementById("message_input").value;
	send = new XMLHttpRequest();
	send.open("POST","messaging/send_message.php",false);
	send.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	send.send("user_id="+user_id+"&message="+message);
	open_messaging_page(user_id);
}
function reload_messages(){

	check = new XMLHttpRequest();
	check.onreadystatechange=function()
 	 {
 	 	if (check.readyState==4 && check.status==200)
	 	   {
			new_message2 = check.responseText;
			if(new_message2=="\ntrue\n"){
				open_messaging_page(message_user_id);
			}
			var timeout_0 = setTimeout(reload_messages, 1000);
	  	   }
 	 }
	check.open("POST","messaging/check_for_new_message.php",true);
	check.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	check.send("user_id="+message_user_id);
}

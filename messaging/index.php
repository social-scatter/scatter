<?php
/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
$type="";
$css="css/main.css";
include "../page_template/check_session.php";

?>
<html>
<base href = "../">
<?php include "../page_template/header_without_js_includes.php"; ?>	<script>message_user_id=0;message_minimized=false;div_number=1;messaging_user_id=-1;</script>
<script src="posts/post.js"></script>
<script src="posts/get_posts.js"></script>
<script src="messaging/open_messaging_page.js"></script>
<script src="users/search_users.js"></script>
<script src="users/get_friends.js"></script>
<script src="posts/post_comment.js"></script>
<script src="posts/get_comments.js"></script>
<script src="misc/jquery-2.1.0.min.js"></script>
<script src="messaging/get_messaging_users.js"></script>
<script src="posts/get_more_posts.js"></script>

<body>
<?php 
include "../page_template/ToC.php"; 
include "../page_template/friends_without_js.php";
?>
<script> get_friends();var timeout_0 = setTimeout(reload_messages, 1000); </script>
<div id="feed">
<div id="spacer">
</div>
<br />

<br />
<br />
<div id="container">
<center><h2>To begin messaging search for a user at the right and click their name</h2></center>
</div>
<div id="spacer4"></div>
<br />
<br />

<br />

</div>
<div id="dynamic_message_input">
</div>
<?php 
include "../page_template/message_box.php"; 
include "../page_template/user_search_input.php";
?>
</body>

</html>

/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
number_of_messages=" ";
function on_click(user_id, what_action){
	if(what_action == "min"){
		name3 = new XMLHttpRequest();
		name3.open("POST","users/get_user.php",false);
		name3.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		name3.send("user_id="+user_id);	
	
		document.getElementById("message_box").innerHTML='<div id="minimized" style="right:15%;" onclick="on_click('+ user_id +' ,' + "'" + 'max' + "'" + ')">'+name3.responseText+'</div>';
	}
	else{
		open_messaging(user_id);
	}

}
function open_messaging(user_id){
	name2 = new XMLHttpRequest();
	name2.open("POST","users/get_user.php",false);
	name2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	name2.send("user_id="+user_id);	
	document.getElementById("message_box").innerHTML='<script>message_user_id='+user_id+'</script><div id="message_box_activated" style="right:15%;"><div id="message_head" onclick="on_click('+ user_id +' ,' + "'" + 'min' + "'" + ')">'+name2.responseText+'</div><div id="messages"></div><input type="text" id="message" placeholder="Message" style="width:100%;" onkeydown="if (event.keyCode == 13) send_message('+ user_id +')"></div>';

	messages = new XMLHttpRequest();
	messages.open("POST","messaging/get_messages.php",false);
	messages.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	messages.send("user_id="+user_id);	
	document.getElementById("messages").innerHTML=messages.responseText;


	var objDiv = document.getElementById("messages");
	objDiv.scrollTop = objDiv.scrollHeight;	
	get_friends();
}

function send_message(user_id){
	var message=document.getElementById("message").value;
	send = new XMLHttpRequest();
	send.open("POST","messaging/send_message.php",false);
	send.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	send.send("user_id="+user_id+"&message="+message);
	open_messaging(user_id);
}

function reload_messages(){
	check = new XMLHttpRequest();
	check.onreadystatechange=function()
 	 {
 	 	if (check.readyState==4 && check.status==200)
	 	   {
			new_message2 = check.responseText;
			if(new_message2=="\ntrue\n"){
				open_messaging(message_user_id);
			}
			var timeout_1 = setTimeout(reload_messages, 1000);
	  	   }
 	 }
	check.open("POST","messaging/check_for_new_message.php",true);
	check.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	check.send("user_id="+message_user_id);
}
function check_new_message(){
	new_messages = new XMLHttpRequest();
	new_messages.onreadystatechange=function()
 	 {
 	 	if (new_messages.readyState==4 && new_messages.status==200)
	 	   {
			new_message = new_messages.responseText;
			if(new_message=="\ntrue\n"){
				how_many();
			}
	  	   }
 	 }
	new_messages.open("POST","messaging/check_for_new_message_from_anyone.php",true);
	new_messages.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	new_messages.send();

}
function how_many(){

	this_many = new XMLHttpRequest();
	this_many.onreadystatechange=function()
 	 {
 	 	if (this_many.readyState==4 && this_many.status==200)
	 	   {
			new_message3 = this_many.responseText;

			if(new_message3 !=number_of_messages){
				get_friends();
				number_of_messages=new_message3;
				
			}
	  	   }
 	 }
	this_many.open("POST","messaging/check_how_many.php",true);
	this_many.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	this_many.send();
}



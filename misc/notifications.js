/*
Scatter - A distributed social network template
Copyright (C) 2014 Scatter
*/
function get_notifications(){
	notifications = new XMLHttpRequest();
	notifications.onreadystatechange=function()
 	 {
 	 	if (notifications.readyState==4 && notifications.status==200)
	 	   {
			document.getElementById('replace').innerHTML=notifications.responseText;
	  	   }
 	 }
	notifications.open("POST","misc/notifications.php",true);
	notifications.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	notifications.send();
}
//above function basically just makes a call to the post.php and get_post.php to get all the relevant data
